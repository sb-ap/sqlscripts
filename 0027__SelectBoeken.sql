USE ModernWays;
SELECT Familienaam, Titel, boeken.Categorie FROM Boeken
    WHERE Categorie = 'Wiskunde'
      AND (Familienaam = 'Dunham' OR Familienaam = 'Hawking');