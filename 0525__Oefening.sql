USE ModernWays;
ALTER TABLE Huisdieren
ADD COLUMN Bewering VARCHAR(30);
SET SQL_SAFE_UPDATES = 0;
UPDATE Huisdieren
SET Bewering = 'X is de naam van een hond'
WHERE Soort = 'hond';
SET SQL_SAFE_UPDATES = 1;