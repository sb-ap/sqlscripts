DROP DATABASE IF EXISTS ModernWays;
CREATE DATABASE IF NOT EXISTS ModernWays;
USE ModernWays;

CREATE TABLE Personen(
Id INT PRIMARY KEY AUTO_INCREMENT,
Voornaam varchar(100) CHARACTER SET utf8mb4 NOT NULL, 
Familienaam varchar(100) CHARACTER SET utf8mb4 NOT NULL,
Geboortejaar SMALLINT NOT NULL
);
CREATE TABLE Films (
Id INT PRIMARY KEY AUTO_INCREMENT,
Naam varchar(100) CHARACTER SET utf8mb4 NOT NULL, 
Jaar SMALLINT NOT NULL
);

CREATE TABLE IMDBscores (
Films_Id INT NOT NULL ,
Score FLOAT(2,1)  NOT NULL,
CONSTRAINT fk_Films_Id  FOREIGN KEY (Films_id)
  REFERENCES Films(Id)
);

CREATE TABLE Ratings (
Personen_Id INT  NOT NULL,
Films_Id INT  NOT NULL,
Rating FLOAT(3,1)  NOT NULL,
CONSTRAINT fk_Personen_Id  FOREIGN KEY (Personen_Id)
  REFERENCES Personen(Id),
CONSTRAINT fk_FilmsRating_Id  FOREIGN KEY (Films_Id)
  REFERENCES Films(Id)
);
INSERT INTO Personen (
Voornaam,
Familienaam,
Geboortejaar
)
VALUES
('Moshe','Sprinzeles',1999),
('Yiddy','Rumpler',1998),
('Chaim','Shonfeld',1995),
('Moshi','Lerner',1997),
('Jacob','Samson',1990),
('Ari','Mandel',1993),
('Esty','Braun',1995),
('Chummi','Apter',1992),
('Ruchy','Green',1996),
('Samuel','Berger',1989);

INSERT INTO Films (
Naam,
Jaar
)
VALUES
('Bad Boys',1995),
('Hobbs & Shaw', 2019),
('Unforgettable',2011),
('NCIS',2003),
('Die Another Day',2002),
('Leverage',2008),
('Quantum of Solace',2008),
('The Spy Who Dumped Me',2018),
('John Wick 1',2014),
('The Black Pirate',1926),
('Bad Boys',1983),
('Fast & Furious 8',2014),
('Brooklyn Nine-Nine',2013),
('NCIS: Los Angeles',2009),
('The Musketeers',2014),
('The Mentalist',2008),
('MacGyver',2016),
('My Spy',2020),
('John Wick: Chapter 2',2017),
('Femme Fatale',2002),
('Bad Boys 2',2003),
('The Blacklist',2013),
('Mr. & Mrs. Smith',2005),
('Castle',2009),
('Casino Royale',2006),
('Killing Eve',2018),
('erry Seinfeld: 23 Hours to Kill',2020),
('Ip Man 4',2019),
('I Still Believe',2020),
('Ip Man 4',2019);

INSERT INTO IMDBscores(
    Films_Id,
    Score
)
VALUES
(1,6.9),
(2,6.5),
(3,6.7),
(4,7.8),
(5,6.1),
(6,7.9),
(7,6.6),
(8,6.0),
(9,7.4),
(10,7.1),
(11,7.2),
(12,4.3),
(13,8.4),
(14,6.7),
(15,7.8),
(16,8.1),
(17,5.2),
(18,6.2),
(19,7.5),
(20,6.2),
(21,6.6),
(22,8.0),
(23,6.5),
(24,8.1),
(25,8.0),
(26,8.3),
(27,6.7),
(28,7.1),
(29,6.5),
(30,7.1);

INSERT INTO Ratings(
    Personen_Id,
    Films_Id,
    Rating
)
VALUES
(1,1,8.1),
(1,11,8),
(1,21,8),
(2,2,10.0),
(2,12,4),
(2,22,6),
(3,3,6),
(3,13,6),
(3,23,7),
(4,4,7),
(4,14,5),
(4,24,3),
(5,5,4),
(5,15,7),
(5,25,3),
(6,6,1),
(6,16,4),
(6,26,6),
(7,7,8),
(7,17,6),
(7,27,8),
(8,8,9),
(8,18,7),
(8,28,9),
(9,9,5),
(9,19,9),
(9,29,7),
(10,10,4),
(10,20,8),
(10,30,6);

/* Opdracht1
SELECT AVG(geboortejaar)AS 'Gemiddelde geboortejaar'
FROM personen;

Opdracht2
SELECT Familienaam, count(*)AS 'Aantal familieleden'
FROM personen
GROUP BY Familienaam;

Opdracht3
SELECT Familienaam, count(*)AS 'Aantal familieleden',avg((2020-geboortejaar)) AS 'Gemiddelde leeftijd'
FROM personen
GROUP BY Familienaam;

Opdracht4
SELECT * FROM films
CROSS JOIN  imdbscores;

Opdracht5
SELECT * FROM ratings
CROSS JOIN  films;

Opdracht6
SELECT * FROM ratings
CROSS JOIN  films,personen;

Opdracht7
SELECT * FROM ratings
CROSS JOIN  films,imdbscores,personen;

Opdracht8
SELECT DISTINCT films.naam AS 'Film',  concat(personen.voornaam, ' ',personen.familienaam)AS 'Recensent',
ratings.Rating AS 'Rating', imdbscores.Score AS 'IMDB Rating'
FROM imdbscores,personen,films,ratings
GROUP BY films.naam,personen.Familienaam;

Opdracht9
USE ModernWays;
SELECT DISTINCT concat(personen.voornaam, ' ',personen.familienaam)AS 'Recensent', 
AVG(ratings.Rating) AS 'AVG(Rating)', AVG(imdbscores.Score) AS 'IMDB Rating',AVG(ABS(Rating-Score)/Score*100)
FROM imdbscores,personen,ratings
GROUP BY Recensent;

Opdracht10
SELECT DISTINCT films.naam AS 'Film', count(ratings.Films_Id) AS 'Aantal keer beoordeeld',
imdbscores.Score AS 'IMDB Rating'
FROM imdbscores,films,ratings
GROUP BY film
ORDER BY 'Aantal keer beoordeeld' ASC,imdbscores.Score DESC,film;




*/




