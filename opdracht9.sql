USE ModernWays;
SELECT DISTINCT concat(personen.voornaam, ' ',personen.familienaam)AS 'Recensent', 
AVG(ratings.Rating) AS 'AVG(Rating)', AVG(imdbscores.Score) AS 'IMDB Rating',AVG(ABS(Rating-Score)/Score*100)
FROM imdbscores,personen,ratings
GROUP BY Recensent;
