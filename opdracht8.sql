USE ModernWays;
SELECT DISTINCT films.naam AS 'Film',  concat(personen.voornaam, ' ',personen.familienaam)AS 'Recensent',
ratings.Rating AS 'Rating', imdbscores.Score AS 'IMDB Rating'
FROM imdbscores,personen,films,ratings
GROUP BY films.naam,personen.Familienaam;