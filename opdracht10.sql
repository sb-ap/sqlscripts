USE ModernWays;
SELECT DISTINCT films.naam AS 'Film', count(ratings.Films_Id) AS 'Aantal keer beoordeeld',
imdbscores.Score AS 'IMDB Rating'
FROM imdbscores,films,ratings
GROUP BY film
ORDER BY 'Aantal keer beoordeeld' ASC,imdbscores.Score DESC,film;

